+++
template = "page_toc.html"
page_template = "page.html"

[extra]
toc_title = "Table of contents"
+++

# Introduction

Let's make a change together!
We have a long road ahead of us, but we believe that something needs to be done.
We won't tolerate the status quo anymore.

# How to support us 

Raising awareness, finding supporters and connecting with politicians, communities and like-minded organizations is crucial for our movement to be successful.
You can help us in many ways, for example by sharing and discussing our ideas with others or contributing to our [website](https://gitlab.com/Safe-Internet-Initiative/website).

Everyone who wants to support us is welcome to 
<a href="#" onclick="join_handler(event)">
    join our team</a>!

# Politicians

It's important to bring up this topic in political debates.
Some countries already plan to solve this issue for themselves by introduce digital age verification.
We don't think this is the right step, but a global solution must be applied that respects privacy.

When it comes to laws that make following the specifications mandatory, please give website owners a reasonable time to react.
Especially websites where users can upload content cannot be made responsible for filtering the content before it is published.
Such websites should have some time to react until unsafe content can be reported and removed or marked as unsafe.

# Website owners

Supporting the specification is very simple for the vast majority of websites. 
According to the current version of the specification, websites don't need to change anything.
However, if they want to be recognized as a "safe" website they should add the `NSC` (not secure content) header with value `0` or `none`.

## Safe websites

Simply add the `NSC` header with value `0` or `none` to all of your HTTP responses.
That's it.

## Unsafe websites

Technically, you don't need to do anything.
Everything that isn't safe is considered unsafe.
However, you should still indicate that your content isn't safe and you can even describe what about the content isn't safe to allow for a more fine-grained filtering.

Simply add the `NSC` header with value `1` or a more suitable value described in the [specification](/specification#4-values-of-the-nsc-header) to all of your HTTP responses.

## Mixed websites

Most websites that serve both safe and unsafe content already have system in place that marks content as "NSFW", for example.
To support the specification you need to mark HTTP responses with the correct `NSC` header value.

## Web-server configuration

Adding the header as a global configuration only requires a single line to be added in Apache or Nginx.
If the web-server you're using is missing in this list, do not hesitate to <a href="#" onclick="contact_handler(event)">contact us</a>.

### Apache

If your website serves only safe content, you can add this to your configuration or root `.htaccess` file:

```
Header add NSC "0"
```

If your website serves mostly unsafe content, you can add this to your configuration or root `.htaccess` file:

```
Header add NSC "1"
```

### Nginx

If your website serves only safe content, you can add this to your configuration:

```
add_header NSC 0;
```

If your website serves mostly unsafe content, you can add this to your configuration:

```
add_header NSC 1;
```
