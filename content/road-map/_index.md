+++
template = "specification.html"
+++

# Road map

We have a long road ahead of us to realize our vision.
We've split our goals into topics we target to achieve soon and things that need to be addressed in the long run.

## Short-term goals

1. Connect with other organizations and individuals that fight for privacy and youth protection.
2. Talk with politicians and raise awareness.
3. Extend our reach.
4. Improve the technical specification.

## Long-term goals

1. Make the specification an international standard.
2. Add support for the specification in open source software and libraries.
3. Campaign to make it illegal to label unsafe content as safe on purpose.
