+++
title = "Introducing the Safe Internet Initiative"
date = 2022-03-10
+++

Recently, several governments have made attempts to protect children from inappropriate content on the internet.
By enforcing laws that have already been in place or by passing new laws, countries like Germany and the UK want to introduce age-verification for adult content.
We agree that something must be done to protect children on the internet, but digital age-verification is a double-edged sword that can significantly harm privacy and free speech.

However, just criticizing plans for digital age-verification won't fix the underlying issue.
We need to offer an alternative approach that protects privacy and children at the same time.
Further, we need a solution that can be a applied globally and not just in a few countries.

### Our solution

We want to introduce a solution that gives everyone full control over which content they want to see.
A solution, that's part of the internet and easy to implement for websites and web browsers alike.

With a simple mechanism, we can allow websites to label their content as safe or unsafe.
Then, according to the preference of the user, unsafe content is shown or hidden.

Our goal is to make blocking unsafe content the default preference.
Yet, everyone will still be able to browse unfiltered content by changing the preference.
Furthermore, parents could use a password to prevent children from changing the preference.

### Now is the time to stand up!

We don't want to leave it to chance whether governments will address this issue and whether they will respect our privacy.
We need to become active now.

Please consider supporting us by joining our team or by raising awareness for this topic.
There are many ways you can help!

+ <a href="#" onclick="join_handler(event)">Join our team</a>
+ <a href="#" onclick="contact_handler(event)">Contact us</a>
+ [Home](/)
+ [Make a change](/make-a-change)
+ [FAQ](/faq)
+ [Technical specification](/specification)
