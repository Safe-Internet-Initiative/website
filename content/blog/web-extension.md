+++
title = "Privacy-friendly youth protection in action"
date = 2022-04-10
+++

When it comes to youth protection, the internet is fundamentally broken.
There is no effective way for a website to tell whether visitors are considered old enough to view their content or not.

The first idea that comes to mind to address this issue is to use age verification.
However, age verification is not as easy to implement digitally as it is in the real world.
In fact, it is virtually impossible to implement digital age verification without implications for online privacy.
And even if a few countries made use of digital age verification, the vast majority of countries still wouldn't have any youth protection.

Fortunately, we don't need a compromise between protection and privacy.

## Introducing our new web extension

We are working on a specification that defines a simple mechanism for privacy-friendly youth protection on the internet.
With our new open-source [web extension](/web-extension) you can try out this specification inside your own web browser right now.
We do not consider this the final implementation. We would rather see this implemented directly in your web browser, but it does show how our specification works in practice.

<video controls>
  <source src="/videos/SII-extension.mp4" type="video/mp4" />
  <source src="/videos/SII-extension.webm" type="video/webm" />
  <p>Your browser doesn't support HTML5 video. Here is a <a href="/videos/SII-extension.mp4">link to the video</a> instead.</p>
</video>

## Challenges ahead

Our website already shows how our specification can be implemented.
Actually, we only had to add a single line to our web-server configuration.

We understand sites with mixed content (Reddit, Twitter, etc.) will need to put more work into implementing our specification.
We're currently working hard to develop concepts that cover all potential use-cases.

Yet, the far bigger challenge is outreach and gaining support of politicians.
We have a long road ahead of us, but we know that something needs to be done.

If we don't stand up now, we leave it up to chance whether the upcoming generations will be protected from harmful content and whether our privacy will be preserved.
