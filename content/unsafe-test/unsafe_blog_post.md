+++
title = "An unsafe blog post"
date = 2022-03-16
+++

# An unsafe blog post

The index file of this block post is labeled as unsafe.
This is not actually unsafe, but can be used as a technical demonstration of our specification.

The fact that you could load this site shows that you haven't enabled blocking of unsafe content.

![Unsafe logo](/unsafe-test/unsafe_logo.svg)
