+++
template = "specification.html"
page_template = "page.html"
+++

# The NSC Blocker web extension

The future is here.
With our new web extension you can try out our specification right now!

You will see, privacy and protection from harmful content can go hand in hand seamlessly.
What's implemented on a only handful of websites yet and still requires a web extension can be part of your web browser and every website you visit eventually.

<a href="https://addons.mozilla.org/en-US/firefox/addon/nsc-blocker" rel="noreferrer noopener" target="_blank">
    <img src="/images/firefox-addon.svg" width="172" alt="Get the Add-on (Firefox)" />
</a>
<a href="https://microsoftedge.microsoft.com/addons/detail/npponfljmogainefpgiliogjdeijlelj" rel="noreferrer noopener" target="_blank">
    <img src="/images/edge-addon.png" width="210" alt="Get the Add-on (Edge)" />
</a>

## How to get started

1. Install the extension.
2. Navigate to [this website](/mixed-test) with mixed content (both safe and unsafe).
3. Navigate to [this website](/unsafe-test) with mainly unsafe content.
4. Change the "Block unsafe responses" setting and try again.

Currently, our website is among the first adopters of the specification on the server side.

**Don't expect this extension to bring any protection against actual unsafe content yet.**

## Screenshots

| Safe page | Unsafe page |
|-|-|
| ![Safe site](safe-site.png) | ![Safe site](unsafe-site.png) |

[**Have a look at the source code!**](https://gitlab.com/Safe-Internet-Initiative/nsc-blocker)
