+++
title = "A mixed (safe and unsafe) blog post"
date = 2022-03-16
+++

# A mixed content blog post

Parts of the content of this post are labeled as unsafe.
This is not actually unsafe, but can be used as a technical demonstration of our specification.

The first icon is labeled as unsafe, the second icon is safe.

If you enabled blocking of unsafe content, you should only see the green image.
If you disabled blocking of unsafe content, you should see the green and the red image.

![Unsafe logo](/unsafe-test/unsafe_logo.svg)
![Safe logo](/icons/logo.svg)
