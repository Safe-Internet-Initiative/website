#!/bin/sh

STATIC_CSS="../../../static/css/"
STATIC_FONTS="../../../static/fonts/"

cd $(dirname "$0")
cd Zulma/scss

# Use node-sass
node-sass default.scss default.css
node-sass dark.scss dark.css

mv default.css $STATIC_CSS
mv dark.css $STATIC_CSS

cp _vendor/MaterialDesign-Webfont/css/materialdesignicons.min.css $STATIC_CSS
cp _vendor/MaterialDesign-Webfont/fonts/* $STATIC_FONTS
