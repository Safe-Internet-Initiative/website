function join_handler(ev) {
  open_email(ev, "Joining the Safe Internet Initiative");
}

function contact_handler(ev) {
  open_email(ev, "Safe Internet Initiative contact");
}

function open_email(ev, subject) {
  ev.preventDefault();
  // To avoid spam...
  const encoded_email = "WVdGeWIyNHVaWEpvWVhKa2RFQnpZV1psTFdsdWRHVnlibVYwTG05eVp3PT0=";
  const email = atob(atob(encoded_email));
  subject = encodeURI(subject);
  window.location.href = `mailto:${email}?subject=${subject}`;
}
