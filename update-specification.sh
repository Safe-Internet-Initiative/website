#!/bin/bash

echo '
+++
template = "specification.html"
page_template = "page.html"
+++
' > ./content/specification/_index.md
cat ./specification/specification.md >> ./content/specification/_index.md
